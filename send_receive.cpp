#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <netinet/in.h>

#include <vector>
#include "sock.h"

#define WILL_DEFAULT 3
#define MAX 10
using namespace std;
struct OLSRHelloMsg {
	char frontReserve[2];
	//:: Emission Interval
	char hTime[1];
	uint8_t willingness;
	char linkCode[1];
	char midReserve[1];
	uint16_t linkMsgSize;
	vector<string> neighbourIA;
};

struct OLSRPacket {
	
	uint16_t packLen;
	uint16_t packSQNum;
	uint8_t messageType;
	uint8_t vTime;
	uint16_t messageSize;
	char sourceAddr[4];
	uint8_t ttl;
	uint8_t hopCount;
	uint16_t messageSQNum;
	struct OLSRHelloMsg helloMsg;
};

struct LinkSet{
	char localIfaceAddr[4];
	char neighbourIfaceAddr[4];
	short symTime;
	short asymTime;
	short lTime;
};

char oneHopNeighbourList[MAX][4];
char twoHopNeighbourList[MAX][4];

struct LinkSet linkTuple[10];
int linkSetCount = 0;
int oneHopNeiCount = 0;
int twoHopNeiCount =0;

int packetSeqCount = 1;

void receive();

 void send2()
 {
  struct Ifconfig ifc;
  init(&ifc);

 struct OLSRPacket packet;

	packet.packLen = sizeof(packet.helloMsg) + 16;
	packet.packSQNum = packetSeqCount;
	packet.messageType = 1;
	packet.vTime = 1;
	packet.messageSize = 12 + sizeof(packet.helloMsg);
	
	char addrSour[4];
	
	int macSeq = 3;
	for(int j=0; j< 4; j++) {
	if(j==0) {
		//strncpy(addrSour[j], '3', 1);
		addrSour[j] = '3';
	} else {
		//strncpy(addrSour[j], ifc.mac[macSeq], 1);
		addrSour[j] = ifc.mac[macSeq];
		macSeq++; 
	}
	
	printf("mac: %s\n",addrSour);
				
	}
	strncpy(packet.sourceAddr, addrSour, sizeof(packet.sourceAddr));
	
	packet.ttl = 1;
	packet.hopCount = 1;
	packet.messageSQNum = 1;
	strncpy((packet.helloMsg).hTime, "0", sizeof((packet.helloMsg).hTime));
	strncpy((packet.helloMsg).frontReserve, "00", sizeof((packet.helloMsg).frontReserve));
	strncpy((packet.helloMsg).midReserve, "0", sizeof((packet.helloMsg).midReserve));
	strncpy((packet.helloMsg).linkCode, "1", sizeof((packet.helloMsg).linkCode));
	(packet.helloMsg).linkMsgSize = 4+ sizeof(packet.helloMsg.neighbourIA);
	(packet.helloMsg).willingness = 3;
	//(packet.helloMsg).neighbourIA.push_back("");
	//(packet.helloMsg).neighbourIA[0] = "4";


  printf("Sending from ");
  /* print the MAC addr */
  int i;
  for (i=0; i<MACADDRLEN-1; i++)
    printf("%02X:", ifc.mac[i]);
  printf("%02X\n", ifc.mac[MACADDRLEN-1]);

  /* set-up destination structure in a sockaddr_ll struct */
  struct sockaddr_ll to;
  memset(&to, 0, sizeof(to)); /* clearing the structure, just in case */
  to.sll_family = AF_PACKET; /* always AF_PACKET */
  to.sll_ifindex = ifc.ifindex;
  to.sll_halen = MACADDRLEN;

  /* setup broadcast address of FF:FF:FF:FF:FF:FF */
  for (i=0; i<MACADDRLEN; i++)
    to.sll_addr[i] = 0xff;

  //char * msg = "Hello world!";


//Network byte conversion


 

char *msg = (char *)malloc(sizeof(packet));
	memcpy(msg, &packet, sizeof(packet));
  int sentlen = sendto(ifc.sockd, msg, sizeof(packet), 0, (struct sockaddr*) &to, sizeof(to));
  printf("%d bytes sent\n", sentlen);
  
sleep(1);
receive();

 destroy(&ifc);


 }


//Receive part
 
 void receive(){
 
  struct Ifconfig ifc;
  init(&ifc);

	struct OLSRPacket recPack;
  int i;
  printf("This Pi is ");
  /* print the MAC addr */
  for (i=0; i<MACADDRLEN-1; i++)
    printf("%02X:", ifc.mac[i]);
  printf("%02X\n", ifc.mac[MACADDRLEN-1]);

  /* setting up the socket address structure for the source of the packet */
  struct sockaddr_ll from;
  socklen_t fromlen = sizeof(from);
  /* setting up the buffer or receiving */
  char * buf = (char*)malloc(ifc.mtu);
  if (buf == NULL)
    die("Cannot allocate receiving buffer\n");

  time_t now = 0;
  time_t now2;
  time(&now2);

  double seconds = difftime(now, now2);

  while (1 && (seconds<5)) {
    int recvlen = recvfrom(ifc.sockd, buf, ifc.mtu, 0, (struct sockaddr*) &from, &fromlen);

    if (recvlen < 0) {
     // printf("Cannot receive data: %s\n", strerror(errno));
    } else if (recvlen < 50) {
      printf("Received %d bytes from ", recvlen);
      for (i=0; i<from.sll_halen-1; i++)
	printf("%02X:", from.sll_addr[i]);
      printf("%02X >\n", from.sll_addr[from.sll_halen-1]);
 
	memcpy(&recPack, buf, sizeof(recPack));
      /* print buffer content */
      //printf("%s\n", buf);

     /*for(int p=0;p<sizeof((recPack.helloMsg).neighbourIA);p++){
        for(int q=0;q<sizeof(recPack.helloMsg).neighbourIA[p];q++)
		printf("Msg originator addr %c", (recPack.helloMsg).neighbourIA[p][q]);
	printf("\n");
      }	*/
	printf("PacketLength %d\n", ntohs(recPack.packLen));
	printf("Time to live %d\n", ntohs(recPack.ttl));
	printf("hop count %d\n", ntohs(recPack.hopCount));
      
      
    }
     time(&now);
     seconds = difftime(now, now2);
  }

  /* free resources */
  free(buf);
   
  sleep(1);
  send2(); 

destroy(&ifc);
}


//Send part 


/* broadcast a hello world packet */
int main() {
 
  //pthread_t thread_1;

  // make threads
   //pthread_create(&thread_1, NULL, receive,NULL);
   //pthread_create(&thread_2, NULL, receive,"");
    //pthread_create(&thread_2, NULL, print_twice, "Bar");

struct Ifconfig ifc;
  init(&ifc);


 struct OLSRPacket packet;

	packet.packLen = sizeof(packet.helloMsg) + 16;
	packet.packSQNum = packetSeqCount;
	packet.messageType = 1;
	packet.vTime = 1;
	packet.messageSize = 12 + sizeof(packet.helloMsg);
	
	char addrSour[4];
	
	int macSeq = 3;
	for(int j=0; j< 4; j++) {
	if(j==0) {
		//strncpy(addrSour[j], '3', 1);
		addrSour[j] = '3';
	} else {
		//strncpy(addrSour[j], ifc.mac[macSeq], 1);
		addrSour[j] = ifc.mac[macSeq];
		macSeq++; 
	}
				
	}
	strncpy(packet.sourceAddr, addrSour, sizeof(packet.sourceAddr));
	
	packet.ttl = 1;
	packet.hopCount = 1;
	packet.messageSQNum = 1;
	strncpy((packet.helloMsg).hTime, "0", sizeof((packet.helloMsg).hTime));
	strncpy((packet.helloMsg).frontReserve, "00", sizeof((packet.helloMsg).frontReserve));
	strncpy((packet.helloMsg).midReserve, "0", sizeof((packet.helloMsg).midReserve));
	strncpy((packet.helloMsg).linkCode, "1", sizeof((packet.helloMsg).linkCode));
	(packet.helloMsg).linkMsgSize = 4+ sizeof(packet.helloMsg.neighbourIA);
	(packet.helloMsg).willingness = 3;
	//(packet.helloMsg).neighbourIA.push_back("");
	//(packet.helloMsg).neighbourIA[0] = "4";


printf("size of packet %d content %d\n", sizeof(packet.packLen), ntohs(packet.packLen));
  printf("Sending from ");
  /* print the MAC addr */
  int i;
  for (i=0; i<MACADDRLEN-1; i++)
    printf("%02X:", ifc.mac[i]);
  printf("%02X\n", ifc.mac[MACADDRLEN-1]);

  /* set-up destination structure in a sockaddr_ll struct */
  struct sockaddr_ll to;
  memset(&to, 0, sizeof(to)); /* clearing the structure, just in case */
  to.sll_family = AF_PACKET; /* always AF_PACKET */
  to.sll_ifindex = ifc.ifindex;
  to.sll_halen = MACADDRLEN;

  /* setup broadcast address of FF:FF:FF:FF:FF:FF */
  for (i=0; i<MACADDRLEN; i++)
    to.sll_addr[i] = 0xff;

  //char * msg = "Hello world!";
char *msg = (char*)malloc(sizeof(packet));
	memcpy(msg, &packet, sizeof(packet));
  int sentlen = sendto(ifc.sockd, msg, sizeof(packet), 0, (struct sockaddr*) &to, sizeof(to));
  printf("%d bytes sent\n", sentlen);
  
sleep(1);
receive();

 destroy(&ifc);

  
return 0;
}


 



 
 
