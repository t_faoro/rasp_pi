CFLAGS=-Wall
CC=g++
LN=g++

# prefix rules
%.o : %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<
	$(CC) $(CFLAGS) -MM -MP -MT $@ $< > $(basename $@).d

SENDRECEIVEOBJ = sock send_receive

all: send_recv

send_recv: $(addsuffix .o, $(SENDRECEIVEOBJ))
	$(LN) -o $@ $^

.PHONY : clean
clean :
	rm -f *.o *.d *~ send_recv

# add dependency info to Makefile
-include $(addsuffix .d,$(basename $(SENDRECEIVEOBJ)))
